package com.example.proyecto.services;

import com.example.proyecto.entities.Category;
import com.example.proyecto.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService{
    private CategoryRepository categoryRepository;

    @Autowired
    @Qualifier(value="categoryRepository")
    public void setCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository=categoryRepository;
    }

    @Override
    public Iterable<Category> listAllCategorys(){
        return  categoryRepository.findAll();
    }
    @Override
    public void saveCategory(Category category){
        categoryRepository.save(category);
    }

    @Override
    public Category getCategory(Integer id){
        return categoryRepository.findById(id).get();
    }
    @Override
    public void deleteCategory(Integer id){
        categoryRepository.deleteById(id);;
    }
}

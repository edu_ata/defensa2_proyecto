package com.example.proyecto.services;

import com.example.proyecto.entities.Pais;

public interface PaisService {
    Iterable<Pais> listAllPaises();

    void savePais(Pais pais);

    Pais getPais(Integer id);
}

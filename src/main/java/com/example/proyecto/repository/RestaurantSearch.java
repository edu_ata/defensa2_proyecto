package com.example.proyecto.repository;

import com.example.proyecto.entities.Restaurant;
//import netgloo.models.User;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public class RestaurantSearch {

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // Spring will inject here the entity manager object
  @PersistenceContext
  private EntityManager entityManager;


  // ------------------------
  // PUBLIC METHODS
  // ------------------------

  public List<Restaurant> search(String text) {
    
    // get the full text entity manager
    FullTextEntityManager fullTextEntityManager =
      org.hibernate.search.jpa.Search.
      getFullTextEntityManager(entityManager);
    
    // create the query using Hibernate Search query DSL
    QueryBuilder queryBuilder =
      fullTextEntityManager.getSearchFactory()
      .buildQueryBuilder().forEntity(Restaurant.class).get();
    
    // a very basic query by keywords
    org.apache.lucene.search.Query query =
      queryBuilder
        .keyword()
        .onFields("name")
        .matching(text)
        .createQuery();

    // wrap Lucene query in an Hibernate Query object
    org.hibernate.search.jpa.FullTextQuery jpaQuery =
      fullTextEntityManager.createFullTextQuery(query, Restaurant.class);
  
    // execute search and return results (sorted by relevance as default)
    @SuppressWarnings("unchecked")
    List<Restaurant> results = jpaQuery.getResultList();
    
    return results;
  } // method search


} // class

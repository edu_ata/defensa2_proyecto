package com.example.proyecto.repository;

import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface RestaurantRepository extends CrudRepository<Restaurant,Integer>{

    /*@Query("select u from Restaurant u where u.name")
    List<Restaurant> findByName(@Param("name")String name);*/
    @Query("select r from Restaurant r where r.text like %:text%")
    Iterable<Restaurant> getRestauranLikeText(@Param("text") String text);

    /*@Query("select r from Restaurant r where r.city like %:text%")
    Iterable<Restaurant> getRestauranPaisCiudad(@Param("text") String text);*/


}

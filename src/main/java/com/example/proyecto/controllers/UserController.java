package com.example.proyecto.controllers;

import com.example.proyecto.entities.Pais;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.entities.User;
import com.example.proyecto.repository.RestaurantSearch;
import com.example.proyecto.services.HibernateSearchService;
import com.example.proyecto.services.PaisService;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserController {


    @Autowired
    private UserService userService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private HibernateSearchService searchservice;
    @Autowired
    private RestaurantSearch restaurantSearch;
    @Autowired
    private PaisService paisService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }
    public void setSearchservice(HibernateSearchService searchservice) {
        this.searchservice = searchservice;
    }
    public void setRestaurantSearch(RestaurantSearch restaurantSearch) {
        this.restaurantSearch = restaurantSearch;
    }
    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String search(@RequestParam(value = "search", required = false) String q,Model model) {
        Iterable<Restaurant> searchResults = null;
        Iterable<Restaurant> restaurantList=restaurantService.listAllRestaurants();
        Iterable<Pais> paisList=paisService.listAllPaises();

        //searchResults = searchservice.fuzzySearch(q);
        //searchResults = restaurantSearch.search(q);

        if(q.equals("")){
            searchResults = restaurantService.listAllRestaurants();

        }else{
            System.out.println("filtro" +q);

            searchResults = restaurantService.getRestauranLikeText(q);
        }

        model.addAttribute("search", searchResults);
        model.addAttribute("restaurantList",restaurantList);
        model.addAttribute("paises",paisList);

        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        return "login";
    }



}
